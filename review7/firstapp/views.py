from django.http import HttpResponse, JsonResponse
from rest_framework.response import Response
from django.shortcuts import render
import requests
import time
from django.core.cache import cache
from rest_framework.decorators import api_view


@api_view(['GET'])
def index(request):
    if request.method=="GET":
        if RedisCache.get("r"):
            r=RedisCache.get("r")
            print("From Cache")
            return JsonResponse(r.json())
        else:
            for i in range(1,5):
                try:
                    r=requests.get("https://reqres.in/api/unknown")
                except:
                    if i==5:
                        break
                    else:
                        time.sleep(i)
                        continue
                if r.status_code==200:
                    RedisCache.set("r",r,30)
                    print("From API")
                    return JsonResponse(r.json())
            return JsonResponse({"detail":"API is Down"})

    
class RedisCache:
    def get(key):
        try:
            opt=cache.get(key)
            return opt
        except:
            return []

    def set(key,value,time):
        try:
            cache.set(key,value,time)
        except:
            return []